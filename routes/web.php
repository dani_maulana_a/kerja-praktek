<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//admin
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function(){
    Route::get('/admin', function () {
        return view('admin/welcome');
    });
    
    Route::resource('products','ProductController');
    Route::resource('categories','CategoryController');
    Route::resource('generals','GeneralController');
    Route::resource('admin', 'VisitorController');
});


//customer
Route::resource('/', 'CustomerController')->names([
    'index' => 'home customer',
    // 'indexkategori' => 'indexkategoricust',
    // 'indexproduk' => 'indexprodukcust',
    // 'indexkontak' => 'indexkontakcust'
]);
// Route::resource('/', 'VisitorController');
Route::get('kategori/', 'CustomerController@indexkategori')->name("kategoricust");
Route::get('kategori/{nama_kategori}/', 'CustomerController@detailkategori')->name("listkategoriproduk");
Route::get('produk/', 'CustomerController@indexproduk')->name("produkcust");
Route::get('produk/{nama_produk}/', 'CustomerController@detailproduk')->name('detailproduk');
Route::get('kontak/', 'CustomerController@indexkontak')->name("kontakcust");
