@extends("admin.layout.conquer")

@section("left_sidebar")
<li class="sidebar-toggler-wrapper">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler">
    </div>
    <div class="clearfix">
    </div>
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
</li>
<li class="sidebar-search-wrapper">
    <form class="search-form" role="form" action="index.html" method="get">
        <div class="input-icon right">
            <i class="icon-magnifier"></i>
            <input type="text" class="form-control" name="query" placeholder="Search...">
        </div>
    </form>
</li>
<li>
    <a href="{{url('/admin')}}">
    <i class="icon-home"></i>
    <span class="title">Dashboard</span>
    <span class="selected"></span>
    </a>
</li>
<li>
    <a href="{{route('categories.index')}}">
        <i class="icon-layers"></i>
        Kategori</a>
    </a>
</li>
<li>
    <a href="{{route('products.index')}}">
        <i class="icon-present"></i>
        Produk</a>
    </a>
</li>
<li class="active">
    <a href="{{route('generals.index')}}">
        <i class="icon-docs"></i>
        Kontak</a>
</li>
@endsection

@section("konten")
<div class="page-content">
<div class="portlet">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-reorder"></i> Ubah Data Kontak
			</div>
		</div>
		<div class="portlet-body form">
			<form method="POST" action="{{ route('generals.update', $data->idgenerals) }}" enctype="multipart/form-data">
			@csrf
			@method("PUT")
				<div class="form-body">
					<div class="form-group">
						<label for="almtGeneral">Alamat</label>
						<input type="text" class="form-control @error('almtGeneral') is-invalid @enderror" name="almtGeneral" value="{{ old('almtGeneral', $data->alamat) }}" placeholder="Isikan alamat Anda">
						@error('almtGeneral')
							<div class="invalid-feedback" style="color:red">{{ $message }}</div>
						@enderror
					</div><br>
					<div class="form-group">
						<label for="almturlGeneral">URL Alamat    &nbsp;(Dapat diambil melalui website <a href="https://google-map-generator.com/">https://google-map-generator.com/</a> dengan cara memasukkan alamat yang diinginkan, klik Get HTML-Code dan tempelkan URL yang berada dalam src)</label>
						<input type="text" class="form-control @error('almturlGeneral') is-invalid @enderror" name="almturlGeneral" value="{{ old('almturlGeneral', $data->alamat_url) }}" placeholder="Contoh: https://maps.google.com/maps?q=2880%20Broadway,%20New%20York&t=&z=13&ie=UTF8&iwloc=&output=embed">
						@error('almturlGeneral')
							<div class="invalid-feedback" style="color:red">{{ $message }}</div>
						@enderror
					</div><br>
                    <div class="form-group">
						<label for="notelpGeneral">Nomor Telepon</label>
						<input type="text" class="form-control @error('notelpGeneral') is-invalid @enderror" name="notelpGeneral" value="{{ old('notelpGeneral', $data->no_telp) }}" placeholder="Isikan nomor telepon Anda">
						@error('notelpGeneral')
							<div class="invalid-feedback" style="color:red">{{ $message }}</div>
						@enderror
					</div><br>
                    <div class="form-group">
						<label for="emailGeneral">E-Mail</label>
						<input type="text" class="form-control @error('emailGeneral') is-invalid @enderror" name="emailGeneral" value="{{ old('emailGeneral', $data->email) }}" placeholder="Isikan E-Mail Anda">
						@error('emailGeneral')
							<div class="invalid-feedback" style="color:red">{{ $message }}</div>
						@enderror
					</div><br>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection