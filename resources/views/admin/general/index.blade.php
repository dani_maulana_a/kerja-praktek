@extends("admin.layout.conquer")

@section("left_sidebar")
<li class="sidebar-toggler-wrapper">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler">
    </div>
    <div class="clearfix">
    </div>
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
</li>
<li class="sidebar-search-wrapper">
    <form class="search-form" role="form" action="index.html" method="get">
        <div class="input-icon right">
            <i class="icon-magnifier"></i>
            <input type="text" class="form-control" name="query" placeholder="Search...">
        </div>
    </form>
</li>
<li>
    <a href="{{url('/admin')}}">
    <i class="icon-home"></i>
    <span class="title">Dashboard</span>
    <span class="selected"></span>
    </a>
</li>
<li>
    <a href="{{route('categories.index')}}">
        <i class="icon-layers"></i>
        Kategori</a>
    </a>
</li>
<li>
    <a href="{{route('products.index')}}">
        <i class="icon-present"></i>
        Produk</a>
    </a>
</li>
<li class="active">
    <a href="{{route('generals.index')}}">
        <i class="icon-docs"></i>
        Kontak</a>
</li>
@endsection

@section("konten")
<div class="page-content">
  <h2>Kontak</h2>
   @if(session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif

  <!-- <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>              -->
  <table class="table">
    @foreach($data as $d)
      <tr>
        <th>Alamat</th>
        <td>{{ $d->alamat }}</td>
      </tr>
      <tr>
        <th>URL Alamat</th>
        <td>{{ $d->alamat_url }}</td>
      </tr>
      <tr>
        <th>Nomor Telepon</th>
        <td>{{ $d->no_telp }}</td>
      </tr>
      <tr>
        <th>E-Mail</th>
        <td>{{ $d->email }}</td>
      </tr>
      <tr>
        <th>Aksi</th>
        <td><a class="btn btn-success" href="{{ route('generals.edit', $d->idgenerals) }}">Ubah</a></td>
      </tr>
    @endforeach
  </table>
</div>
@endsection