<!-- <!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
<!-- </head>
<body>

<div class="container"> -->
@extends("admin.layout.conquer")

@section("left_sidebar")
<li class="sidebar-toggler-wrapper">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler">
    </div>
    <div class="clearfix">
    </div>
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
</li>
<li class="sidebar-search-wrapper">
    <form class="search-form" role="form" action="index.html" method="get">
        <div class="input-icon right">
            <i class="icon-magnifier"></i>
            <input type="text" class="form-control" name="query" placeholder="Search...">
        </div>
    </form>
</li>
<li>
    <a href="{{url('/admin')}}">
    <i class="icon-home"></i>
    <span class="title">Dashboard</span>
    <span class="selected"></span>
    </a>
</li>
<li>
    <a href="{{route('categories.index')}}">
        <i class="icon-layers"></i>
        Kategori</a>
    </a>
</li>
<li class="active">
    <a href="{{route('products.index')}}">
        <i class="icon-present"></i>
        Produk</a>
    </a>
</li>
<li >
    <a href="{{route('generals.index')}}">
        <i class="icon-docs"></i>
        Kontak</a>
</li>
@endsection

@section("konten")
<div class="page-content">
<div class="portlet">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-reorder"></i> Ubah Data Produk
			</div>
		</div>
		<div class="portlet-body form">
			<form method="POST" action="{{ route('products.update', $data->idproducts) }}" enctype="multipart/form-data">
			@csrf
			@method("PUT")
				<div class="form-body">
					<div class="mb-3">
						<label for="image" class="form-label">Gambar</label> 
						<input type="hidden" name="oldImage" value="{{ $data->gambar_produk }}"> <!-- menyimpan image lama -->
						<input class="form-control" type="file" id="image" name="image" onchange="document.getElementById('img-preview').src = window.URL.createObjectURL(this.files[0])">
						@if($data->gambar_produk)
							<img src="{{ asset('storage/' . $data->gambar_produk) }}" class="img-fluid" id="img-preview" style="max-height:400px">
						@else
							<img class="img-fluid" id="img-preview" style="max-height:400px">
							@error('image')
							<div class="invalid-feedback" style="color:red">{{ $message }}</div>
						@enderror
						@endif
					</div><br>
					<div class="form-group">
						<label for="nmProduk">Nama</label>
						<input type="text" class="form-control @error('nmProduk') is-invalid @enderror" name="nmProduk" value="{{ old('nmProduk', $data->nama_produk) }}" placeholder="Isikan nama produk Anda">
						@error('nmProduk')
							<div class="invalid-feedback" style="color:red">{{ $message }}</div>
						@enderror
					</div><br>
					<div class="form-group">
						<label for="hrgProduk">Harga</label> (Rp)
						<input type="number" class="form-control @error('hrgProduk') is-invalid @enderror" name="hrgProduk" value="{{ old('hrgProduk', $data->harga_produk) }}"  placeholder="Isikan harga produk Anda" min="0">
						@error('hrgProduk')
							<div class="invalid-feedback" style="color:red">{{ $message }}</div>
						@enderror
					</div><br>
					<div class="form-group">
						<label for="deskProduk">Deskripsi</label>
						<textarea class="form-control @error('deskProduk') is-invalid @enderror" name="deskProduk" placeholder="Isikan deskripsi produk Anda" rows="3">{{ old('deskProduk', $data->deskripsi_produk) }}</textarea>
						@error('deskProduk')
							<div class="invalid-feedback" style="color:red">{{ $message }}</div>
						@enderror
					</div><br>
					<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label">Status Ketersediaan</label>
						<div class="col-md-12">
							<select name="ktrsProduk" id="ktrsProduk" class="form-control">
								<option value="tersedia" {{ old('ktrsProduk', $data->ketersediaan == 'tersedia' ? 'selected' : '' ) }}>Tersedia</option>
								<option value="tidak tersedia" {{ old('ktrsProduk', $data->ketersediaan == 'tidak tersedia' ? 'selected' : '' ) }}>Tidak Tersedia</option>
							</select>
						</div>
                	</div><br>
					<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label">Tampilkan Produk pada Halaman Konsumen</label>
						<div class="col-md-12">
							<select name="tplProduk" id="tplProduk" class="form-control">
								<option value="ya" {{ old('tplProduk', $data->tampil == 'ya' ? 'selected' : '' ) }}>Ya</option>
								<option value="tidak" {{ old('tplProduk', $data->tampil == 'tidak' ? 'selected' : '' ) }}>Tidak</option>
							</select>
						</div>
                	</div><br>
						<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label">Kategori Produk</label>
						<div class="col-md-12">
							<select name="ktgrProduk" id="ktgrProduk" class="form-control @error('ktgrProduk') is-invalid @enderror">
								<option value="" >== Pilih Kategori ==</option>
								@foreach($datakategori as $dk)
								<option value="{{ $dk->idcategories }}" {{ old('ktgrProduk', $data->categories_id) == $dk->idcategories ? 'selected' : null }}>{{ $dk->nama_kategori }}</option>
								@endforeach
							</select>
							@error('ktgrProduk')
								<div class="invalid-feedback" style="color:red">{{ $message }}</div>
							@enderror
						</div>
                	</div>
				</div>
				<!--  -->
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
<!-- </body>
</html> -->