<!-- <!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
<!-- </head>
<body>

<div class="container"> -->
@extends("admin.layout.conquer")

@section("left_sidebar")
<li class="sidebar-toggler-wrapper">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler">
    </div>
    <div class="clearfix">
    </div>
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
</li>
<li class="sidebar-search-wrapper">
    <form class="search-form" role="form" action="index.html" method="get">
        <div class="input-icon right">
            <i class="icon-magnifier"></i>
            <input type="text" class="form-control" name="query" placeholder="Search...">
        </div>
    </form>
</li>
<li>
    <a href="{{url('/admin')}}">
    <i class="icon-home"></i>
    <span class="title">Dashboard</span>
    <span class="selected"></span>
    </a>
</li>
<li class="active">
    <a href="{{route('categories.index')}}">
        <i class="icon-layers"></i>
        Kategori</a>
    </a>
</li>
<li>
    <a href="{{route('products.index')}}">
        <i class="icon-present"></i>
        Produk</a>
    </a>
</li>
<li >
    <a href="{{route('generals.index')}}">
        <i class="icon-docs"></i>
        Kontak</a>
</li>
@endsection

@section("konten")
<div class="page-content">
<div class="portlet">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-reorder"></i> Ubah Data Kategori
			</div>
		</div>
		<div class="portlet-body form">
			<form method="POST" action="{{ route('categories.update', $data->idcategories) }}" enctype="multipart/form-data">
			@csrf
			@method("PUT")
				<div class="form-body">
					<div class="mb-3">
                        <label for="image" class="form-label">Gambar</label> 
                        <input type="hidden" name="oldImage" value="{{ $data->gambar_kategori }}"> 
                        <input class="form-control @error('image') is-invalid @enderror" type="file" id="image" name="image"  onchange="document.getElementById('img-preview').src = window.URL.createObjectURL(this.files[0])">
                        @if($data->gambar_kategori)
                            <img src="{{ asset('storage/' . $data->gambar_kategori) }}" class="img-fluid" id="img-preview" style="max-height:400px">
                      	@else
                            <img class="img-fluid" id="img-preview" style="max-height:400px">
                            @error('image')
                                <div class="invalid-feedback" style="color:red">{{ $message }}</div>
                            @enderror
                        @endif
                    </div><br> 
					<div class="form-group">
						<label for="nmKategori">Nama</label>
						<input type="text" class="form-control @error('nmKategori') is-invalid @enderror" name="nmKategori" value="{{ old('nmKategori', $data->nama_kategori) }}" placeholder="Isikan nama kategori Anda">
						@error('nmKategori')
							<div class="invalid-feedback" style="color:red">{{ $message }}</div>
						@enderror
					</div><br>
					<div class="form-group">
						<label for="deskKategori">Deskripsi</label>
						<textarea class="form-control @error('deskKategori') is-invalid @enderror" name="deskKategori" placeholder="Isikan deskripsi kategori Anda" rows="3">{{ old('deskKategori', $data->deskripsi_kategori) }}</textarea>
						@error('deskKategori')
							<div class="invalid-feedback" style="color:red">{{ $message }}</div>
						@enderror
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
<!-- </body>
</html> -->