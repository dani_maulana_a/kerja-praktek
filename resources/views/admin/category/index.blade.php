<!-- <!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
<!-- </head>
<body>

<div class="container"> -->
@extends("admin.layout.conquer")

@section("left_sidebar")
<li class="sidebar-toggler-wrapper">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler">
    </div>
    <div class="clearfix">
    </div>
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
</li>
<li class="sidebar-search-wrapper">
    <form class="search-form" role="form" action="index.html" method="get">
        <div class="input-icon right">
            <i class="icon-magnifier"></i>
            <input type="text" class="form-control" name="query" placeholder="Search...">
        </div>
    </form>
</li>
<li>
    <a href="{{url('/admin')}}">
    <i class="icon-home"></i>
    <span class="title">Dashboard</span>
    <span class="arrow"></span>
    </a>
</li>
<li class="active">
    <a href="{{route('categories.index')}}">
        <i class="icon-layers"></i>
        Kategori</a>
    </a>
</li>
<li>
    <a href="{{route('products.index')}}">
        <i class="icon-present"></i>
        Produk</a>
    </a>
</li>
<li >
    <a href="{{route('generals.index')}}">
        <i class="icon-docs"></i>
        Kontak</a>
</li>
@endsection

@section("konten")
<div class="page-content">
  <h2>Daftar Kategori</h2>
  <div>
	      <a type= "button" href="{{route('categories.create')}}" class="btn btn-fit-height default">
					+ TAMBAH DAFTAR KATEGORI
				</a>
   </div>

   @if(session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif

  @if (session('statushapus'))
    <div class="alert alert-danger">
      {{ session('statushapus') }}
    </div>
  @endif

  <!-- <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>              -->
  <table class="table">
    <thead>
      <tr>
        <!-- <th>ID</th> -->
        <th>Gambar</th>
        <th>Nama</th>
        <th>Deskripsi</th>
        <th>Daftar Produk</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $d) 
      <tr>
        <!-- <td>{{ $d->idcategories }}</td> -->
        <td>
          @if ($d->gambar_kategori)
            <button type="button" style="background-color: Transparent; background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden;">
              <img src="{{ asset('storage/'.$d->gambar_kategori) }}" style="max-height:100px" alt="" data-toggle="modal" href="#detail_{{$d->idcategories}}">
            </button>
            <div class="modal fade" id="detail_{{$d->idcategories}}" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title">{{ $d->nama_kategori }}</h4>
                    </div>
                    <div class="modal-body">
                      <img src="{{ asset('storage/'.$d->gambar_kategori) }}" style="width: 60%; display: block; margin-left: auto; margin-right: auto;" alt=""/>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                  </div>
              </div>
            </div>
          @endif
        </td>
        <td>{{ $d->nama_kategori }}</td>
        <td>{{ $d->deskripsi_kategori }}</td>
        <td>
          @foreach($d->products as $p) 
            {{ $p->nama_produk }};
          @endforeach
        </td>
        <td>
          <a class="btn btn-success" href="{{ route('categories.edit', $d->idcategories) }}">Ubah</a> <br> <br>
          <form method="POST" action="{{route('categories.destroy' , $d->idcategories)}}">
            @method('DELETE')
            @csrf
            <input class="btn btn-danger" type="SUBMIT" value="Hapus"
              onclick="if(!confirm('Apakah Anda yakin?')) {return false;}">
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection

<!-- </body>
</html> -->