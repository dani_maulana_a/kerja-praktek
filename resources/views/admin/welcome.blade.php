@extends("admin.layout.conquer")

@section("left_sidebar")
<li class="sidebar-toggler-wrapper">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler">
    </div>
    <div class="clearfix">
    </div>
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
</li>
<li class="sidebar-search-wrapper">
    <form class="search-form" role="form" action="index.html" method="get">
        <div class="input-icon right">
            <i class="icon-magnifier"></i>
            <input type="text" class="form-control" name="query" placeholder="Search...">
        </div>
    </form>
</li>
<li class="start active ">
    <a href="{{url('/') }}">
    <i class="icon-home"></i>
    <span class="title">Dashboard</span>
    <span class="selected"></span>
    </a>
</li>
<li>
    <a href="{{route('categories.index')}}">
        <i class="icon-layers"></i>
        Kategori</a>
    </a>
</li>
<li>
    <a href="{{route('products.index')}}">
        <i class="icon-present"></i>
        Produk</a>
    </a>
</li>
<li >
    <a href="{{route('generals.index')}}">
        <i class="icon-docs"></i>
        Kontak</a>
</li>
@endsection

@section("konten")


<div class="page-content">
    <div id="chartvisitors">

    </div><br>
    <div class="row">
				<div class="col-md-6">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Pengunjung Halaman Utama
							</div>
						</div>
						<div class="portlet-body">
							<div id="charthomecust">
                            </div>
						</div>
					</div>
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Pengunjung Halaman Produk
							</div>
						</div>
						<div class="portlet-body">
                            <div id="chartprodukcust">
                            </div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Pengunjung Halaman Kategori
							</div>
						</div>
						<div class="portlet-body">
                            <div id="chartkategoricust">
                            </div>
						</div>
					</div>
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Pengunjung Halaman Kontak
							</div>
						</div>
						<div class="portlet-body">
                            <div id="chartkontakcust">
                            </div>
						</div>
					</div>
				</div>
			</div>
</div>
@endsection

@section('footer')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
Highcharts.chart('chartvisitors', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Pengunjung Website Keseluruhan'
    },
    // subtitle: {
    //     text: 'Source: WorldClimate.com'
    // },
    xAxis: {
        categories: {!!json_encode($categories)!!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        //     '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Murni',
        data: {!!json_encode($data_seluruh_m)!!}

    }, {
        name: 'Berdasarkan IP Address',
        data: {!!json_encode($data_seluruh_ip)!!}

    }, {
        name: 'Berdasarkan Session',
        data: {!!json_encode($data_seluruh_session)!!}

    }]
});

Highcharts.chart('charthomecust', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: {!!json_encode($categories)!!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Murni',
        data: {!!json_encode($data_home)!!}

    }, {
        name: 'Berdasarkan IP Address',
        data: {!!json_encode($data_home_ip)!!}

    }, {
        name: 'Berdasarkan Session',
        data: {!!json_encode($data_home_session)!!}

    }]
});

Highcharts.chart('chartprodukcust', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: {!!json_encode($categories)!!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Murni',
        data: {!!json_encode($data_produk)!!}

    }, {
        name: 'Berdasarkan IP Address',
        data: {!!json_encode($data_produk_ip)!!}

    }, {
        name: 'Berdasarkan Session',
        data: {!!json_encode($data_produk_session)!!}

    }]
});

Highcharts.chart('chartkategoricust', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: {!!json_encode($categories)!!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Murni',
        data: {!!json_encode($data_kategori)!!}

    }, {
        name: 'Berdasarkan IP Address',
        data: {!!json_encode($data_kategori_ip)!!}

    }, {
        name: 'Berdasarkan Session',
        data: {!!json_encode($data_kategori_session)!!}

    }]
});

Highcharts.chart('chartkontakcust', {
    chart: {
        type: 'column'
    },
    title: {
        text: ' '
    },
    xAxis: {
        categories: {!!json_encode($categories)!!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Murni',
        data: {!!json_encode($data_kontak)!!}

    }, {
        name: 'Berdasarkan IP Address',
        data: {!!json_encode($data_kontak_ip)!!}

    }, {
        name: 'Berdasarkan Session',
        data: {!!json_encode($data_kontak_session)!!}

    }]
});
</script>
@stop