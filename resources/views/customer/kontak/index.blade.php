@extends("customer.layout.partex")

@section("banneritem")
  <!-- Banner Item Start -->
<div class="sigma_banner-text">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-lg-8 text-center">
          <div class="mb-0 section-title">
            <h1 class="text-white title" style="text-align:center; word-wrap: break-word;" >KONTAK KAMI</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Banner Item End -->
@endsection

@section("kontencust");
<!-- Icons Start -->
<div class="section section-padding">
    <div class="container">

      <div class="section-title text-center">
        <h6 class="subtitle">INFORMASI</h6>
        <h4 class="title">Tetap terhubung dengan kami!</h4>
        <!-- <p>Lorem ipsum dolor sit amet consectetur adipi sicing elit, sed do eiusmod tempor incididut labore et dolore magna aliqua.</p> -->
      </div>
      @foreach($general as $g)
      <div class="row">

        <div class="col-lg-4 col-md-6">
          <div class="sigma_icon-block icon-block-7">
            <i class="flaticon-call"></i>
            <div class="sigma_icon-block-content">
              <h5>Nomor Telepon</h5>
              <p>{{ $g->no_telp }}</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6">
          <div class="sigma_icon-block icon-block-7">
            <i class="flaticon-message"></i>
            <div class="sigma_icon-block-content">
              <h5>E-Mail</h5>
              <p>{{ $g->email }}</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6">
          <div class="sigma_icon-block icon-block-7">
            <i class="flaticon-paper-plane"></i>
            <div class="sigma_icon-block-content">
              <h5>Alamat</h5>
              <p>{{ $g->alamat }}</p>
            </div>
          </div>
        </div>

      </div>
      @endforeach
    </div>
  </div>
  <!-- Icons End -->

  <!-- Map Start -->
<div class="section pt-0">
    <div class="container" >
    @foreach($general as $g)
      <div class="sigma_map" >
      <iframe width="600" height="500" id="gmap_canvas" src="{{ $g->alamat_url }}" frameborder="0" scrolling="no" marginheight="100" marginwidth="200"></iframe>
      </div>
    @endforeach
    </div>
  </div>
<!-- Map End -->
@endsection