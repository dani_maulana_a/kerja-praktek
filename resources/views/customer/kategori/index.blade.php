@extends("customer.layout.partex")

@section("banneritem")
  <!-- Banner Item Start -->
<div class="sigma_banner-text">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-lg-8 text-center">
          <div class="mb-0 section-title">
            <h1 class="text-white title" style="text-align:center; word-wrap: break-word;" >DAFTAR KATEGORI</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Banner Item End -->
@endsection

@section("kontencust");
 <!-- Services Start -->
 <div class="section section-padding">
    <div class="container">
        <div class="row">
            @foreach($category as $c)
            <div class="col-xl-3 col-lg-4 col-md-6">
                <a href="kategori/{{$c->nama_kategori}}" class="sigma_service style-2">
                    <div class="sigma_service-thumb">
                    <img src="{{ asset('storage/'.$c->gambar_kategori) }}" alt="service">
                    </div>
                    <div class="sigma_service-body">
                        <h5>{{ $c->nama_kategori }}</h5>
                        <p>{{ $c->deskripsi_kategori }}</p>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Services End -->
@endsection