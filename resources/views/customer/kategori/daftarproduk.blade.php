@extends("customer.layout.partex")

@section("banneritem")
<!-- Banner Item Start -->
<div class="sigma_banner-text">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-lg-8 text-center">
          <div class="mb-0 section-title">
            <h6 class="subtitle">Daftar Produk Dengan Kategori</h6>
            <h1 class="text-white title" style="text-align:center; word-wrap: break-word;" >"{{ $nama_kategori }}"</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Banner Item End -->
@endsection

@section("kontencust");
<p style="text-align:center;">Terdapat <b>{{count($result)}} </b>produk pada kategori {{ $nama_kategori }}</p>
<div class="section">
    <div class="container">
      <div class="row">
      @foreach($result as $d)
        <!-- Article Start -->
        <div class="col-lg-4 col-md-6">
          <article class="sigma_post">
            <div class="sigma_post-thumb">
              <a href="{{ route('detailproduk', $d->nama_produk) }}">
                <img src="{{ asset('storage/'.$d->gambar_produk) }}" alt="post">
              </a>
            </div>
            <div class="sigma_post-body">
              <div class="sigma_post-meta">
                <div class="sigma_post-categories">
                  <a href="{{ route('detailproduk', $d->nama_produk) }}" class="sigma_post-category">{{ $d->ketersediaan }}</a>
                </div>
                <div class="sigma_product-price">
                  <span>Rp. {{$d->harga_produk}}</span>
                </div>
              </div>
              <h5> <a href="{{ route('detailproduk', $d->nama_produk) }}">{{ $d->nama_produk }}</a> </h5>
            </div>
          </article>
        </div>
        <!-- Article End -->
      @endforeach
      </div> 
    </div>
</div>
@endsection