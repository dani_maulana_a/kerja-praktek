@extends("customer.layout.partex")

@section("banneritem")
  <!-- Banner Item Start -->
<div class="sigma_banner-text">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-lg-8 text-center">
          <div class="mb-0 section-title">
            <h1 class="text-white title" style="text-align:center; word-wrap: break-word;" >DETAIL PRODUK</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Banner Item End -->
@endsection

@section("kontencust");
<!-- Product Content Start -->
<div class="section">
    <div class="container">
    
      <div class="row">
        <div class="col-md-6">
          <div class="sigma_product-single-thumb">
            <img src="{{ asset('storage/'.$product->gambar_produk) }}" alt="product">
          </div>

        </div>
        <div class="col-md-6">

          <div class="sigma_product-single-content">

            <h4 class="entry-title">{{ $product->nama_produk }}</h4>

            <div class="sigma_product-price">
              <span>Rp. {{ $product->harga_produk }}</span>
            </div>

            <p>
              <strong>Ketersediaan: <span>{{ $product->ketersediaan }}</span></strong> 
            </p>

            <p class="sigma_product-excerpt">{{ $product->deskripsi_produk }}</p>

            <form class="sigma_product-atc-form">
              <div class="qty-outter">
                <a href="https://wa.me/62{{ $general->no_telp }}?text=Halo%20admin,%20saya%20ingin%20bertanya%20produk%20{{ $product->nama_produk }}" class="sigma_btn-custom secondary btn-pill">Beli Sekarang</a>
              </div>
            </form>

          </div>

        </div>
      </div>

    </div>
  </div>
  <!-- Product Content End -->
@endsection