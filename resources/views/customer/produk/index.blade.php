@extends("customer.layout.partex")

@section("banneritem")
  <!-- Banner Item Start -->
<div class="sigma_banner-text">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-lg-8 text-center">
          <div class="mb-0 section-title">
            <h1 class="text-white title" style="text-align:center; word-wrap: break-word;" >DAFTAR PRODUK</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Banner Item End -->
@endsection

@section("kontencust");
<div class="section">
    <div class="container">
      <div class="row masonry">
        <!-- Product Start -->
        @foreach($product as $p)
        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 masonry-item">
          <div class="sigma_product">
            <div class="sigma_product-thumb">
              <a href="{{ route('detailproduk', $p->nama_produk) }}"><img src="{{ asset('storage/'.$p->gambar_produk) }}" alt="product"></a>
            </div>
            <div class="sigma_product-body">
              <h5 class="sigma_product-title"> <a href="{{ route('detailproduk', $p->nama_produk) }}">{{ $p->nama_produk }}</a> </h5>
              <div class="sigma_product-price">
                <span>{{ $p->harga_produk }}</span>
              </div>
              <a href="{{ route('detailproduk', $p->nama_produk) }}" class="sigma_btn-custom btn-sm dark btn-pill">Cek Detail</a>
            </div>
          </div>
        </div>
        @endforeach
        <!-- Product End -->
      </div>
    </div>
</div>
@endsection