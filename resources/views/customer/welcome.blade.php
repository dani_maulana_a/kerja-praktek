@extends("customer.layout.partex")

@section("banneritem")
<!-- Banner Item Start -->
  <div class="sigma_banner-text">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-lg-8 text-center">
          <div class="mb-0 section-title">
            <h6 class="subtitle">Tentang Kami</h6>
            <h1 class="text-white title" style="text-align:center; word-wrap: break-word;" >Melayani segala kebutuhan digital printing untuk Anda dengan kualitas terbaik</h1>
            <!-- <p class="text-white">Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Banner Item End -->
@endsection

@section("kontencust");
<!-- Services Start -->
  <div class="section section-padding">
    <div class="container">

      <div class="section-title text-center">
        <h1 class="title">Kategori Produk</h1>
        <!-- <p>Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
      </div>

      <div class="row">
      @foreach($category as $c) 
        <div class="col-xl-3 col-lg-4 col-md-6">
          <a href="kategori/{{$c->nama_kategori}}" class="sigma_service style-2">
            <div class="sigma_service-thumb">
            @if ($c->gambar_kategori)
              <img src="{{ asset('storage/'.$c->gambar_kategori) }}" alt="">
            @endif
            </div>
            <div class="sigma_service-body">
              <h5>{{ $c->nama_kategori }}</h5>
              <p>{{ $c->deskripsi_kategori }}</p>
            </div>
          </a>
        </div>
        @endforeach
      </div>
  </div><br><br><br><br>
<!-- Services End -->


<!-- Related Products start -->
<div class="section pt-0">
    <div class="container">
      <div class="section-title text-center">
        <h1 class="title">Daftar Produk</h1>
        <!-- <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p> -->
      </div>
      <div class="sigma_related-posts">
        <!-- Products Post Start -->
        <div class="sigma_related-slider">
        @foreach($product as $p) 
          <!-- Product Start -->
          <div class="sigma_product">
            <div class="sigma_product-thumb">
              <a href="{{ route('detailproduk', $p->nama_produk) }}"><img src="{{ asset('storage/'.$p->gambar_produk) }}" alt="product"></a>
            </div>
            <div class="sigma_product-body">
              <h5 class="sigma_product-title"> <a href="{{ route('detailproduk', $p->nama_produk) }}">{{ $p->nama_produk }}</a> </h5>
              <div class="sigma_product-price">
                <span>Rp. {{ $p->harga_produk }}</span>
              </div>
              <!-- <a href="/produk/{{$p->nama_produk}}" class="sigma_btn-custom btn-sm dark btn-pill">Cek Detail</a> -->
              <a href="{{ route('detailproduk', $p->nama_produk) }}" class="sigma_btn-custom btn-sm dark btn-pill">Cek Detail</a>
            </div>
          </div>
          <!-- Product End -->
        @endforeach
        </div>
        <!-- Products End -->
      </div>
    </div>
  </div>
<!-- Related Products End -->

<!-- Map Start -->
<div class="section pt-0">
    <div class="container">
    @foreach($general as $g)
      <div class="sigma_map">
      <iframe width="600" height="500" id="gmap_canvas" src="{{ $g->alamat_url }}" frameborder="0" scrolling="no" marginheight="100" marginwidth="200"></iframe>
        <div class="sigma_contact-info" style="width: 420px;">
          <h3 class="text-white">Kontak Kami</h3>
          <div class="sigma_contact-info-item">
            <h6>Lokasi</h6>
            <p>{{ $g->alamat }}</p>
          </div>
          <div class="sigma_contact-info-item">
            <h6>E-Mail</h6>
            <p>{{ $g->email }}</p>
          </div>
          <div class="sigma_contact-info-item">
            <h6>Nomor Telepon</h6>
            <p>{{ $g->no_telp }}</p>
          </div>
          <!-- <div class="sigma_contact-info-item">
            <ul class="sigma_sm">
              <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
              <li> <a href="#"> <i class="fab fa-instagram"></i> </a> </li>
              <li> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
              <li> <a href="#"> <i class="fab fa-linkedin-in"></i> </a> </li>
            </ul>
          </div> -->
        </div>
      </div>
      @endforeach
    </div>
  </div>
<!-- Map End -->
@endsection