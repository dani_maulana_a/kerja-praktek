<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    // primary key pada database yaitu idproducts (default dari laravel yaitu id)
    protected $primaryKey = 'idproducts';

     // di product kita bisa ambil category, karena 1 produk punya 1 kategori maka pake belongsTo
    // nama fungsi category bebas, yang penting esensinya satu kategori 
    public function category() {
        //parameter berisi namespace\namaModel
        return $this->belongsTo("App\Category","category_id");
    }

    public $timestamps = false; //biar nggak error pas tambah supplier karena default dari laravel pakai timestamp, sedangkan kita ngga pakai timestamp
}
