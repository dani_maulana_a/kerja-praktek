<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    // primary key pada database yaitu idcategories (default dari laravel yaitu id)
    protected $primaryKey = 'idcategories';

    // di category kita bisa ambil products (pake s krn banyak), karena satu kategori banyak products maka pake hasMany
    // nama fungsi products bebas, yang penting esensinya kumpulan products 
    public function products() {
        //parameter berisi namespace\namaModel
        return $this->hasMany("App\Product","categories_id","idcategories");
    }

    public $timestamps = false; //biar nggak error pas tambah supplier karena default dari laravel pakai timestamp, sedangkan kita ngga pakai timestamp
}