<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class General extends Model
{
    protected $table = 'generals';

    // primary key pada database yaitu generals (default dari laravel yaitu id)
    protected $primaryKey = 'idgenerals';

    public $timestamps = false;
}
