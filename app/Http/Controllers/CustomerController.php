<?php

namespace App\Http\Controllers;
use App\Category;
use App\Product;
use App\General;
use App\Visitor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

use Routes\web;
use Illuminate\Support\Facades\Route;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visitor = new Visitor();

        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $visitor->ip = $ip;

        session_start();
        $visitor->session = session_id();

        $name = Route::currentRouteName();
        $visitor->halaman = $name;
        //HASILNYA {"ip":"127.0.0.1","session":"57cdkvhuk5n2rfs5nlflritc9a","halaman":"visitor.index"}

        // echo "$visitor";
        $visitor->save();

        $category = Category::all();
        $product = DB::table('products')
                    ->join("categories","products.categories_id","=","categories.idcategories")
                    ->where("products.tampil","=","ya")
                    ->get();
        $general = General::all();
        
        return view('customer.welcome', ['category' => $category, 'product' => $product, 'general' => $general]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexkategori()
    {
        $visitor = new Visitor();

        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $visitor->ip = $ip;

        session_start();
        $visitor->session = session_id();

        $name = Route::currentRouteName();
        $visitor->halaman = $name;
        //HASILNYA {"ip":"127.0.0.1","session":"57cdkvhuk5n2rfs5nlflritc9a","halaman":"visitor.index"}

        // echo "$visitor";
        $visitor->save();

        $category = Category::all();

        return view('customer.kategori.index', ['category' => $category]);
    }

    //detail kategori
    public function detailkategori($nama_kategori)
    {
       $category = DB::table('categories')
                    ->join('products','categories.idcategories','=','products.categories_id')
                    ->where('categories.nama_kategori',$nama_kategori)
                    ->where("products.tampil","=","ya")
                    ->get();

       $result = $category; //list/array products nanti di view harus foreach krn array

       return view('customer.kategori.daftarproduk',compact('nama_kategori','result'));
    }

    //halaman daftar produk
    public function indexproduk()
    {
        $visitor = new Visitor();

        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $visitor->ip = $ip;

        session_start();
        $visitor->session = session_id();

        $name = Route::currentRouteName();
        $visitor->halaman = $name;

        $visitor->save();

        $product = DB::table('products')
                    ->join("categories","products.categories_id","=","categories.idcategories")
                    ->where("products.tampil","=","ya")
                    ->get();

        return view('customer.produk.index', ['product' => $product]);
    }

    public function detailproduk($nama_produk)
    {
        $general = General::all()->first();
        $product = Product::where('nama_produk', $nama_produk)->first();

        return view('customer.produk.detail', ['product' => $product, 'general' => $general]);
    }

    //halaman kontak kami
    public function indexkontak()
    {
        $visitor = new Visitor();

        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $visitor->ip = $ip;

        session_start();
        $visitor->session = session_id();

        $name = Route::currentRouteName();
        $visitor->halaman = $name;

        $visitor->save();

        $general = General::all();

        return view('customer.kontak.index', ['general' => $general]);
    }

    //halaman kontak kami
    public function layoutkontak()
    {
        $general = General::all();

        return view('customer.layout.partex', ['general' => $general]);
    }
}
