<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Query bulider
        $queryBuilder = DB::table('products')
                        ->join("categories","products.categories_id","=","categories.idcategories")->get();

        //Passing data ke view
        //Cara 2 dengan sintaks array. Berarti variabel queryBuilder nanti hanya dikenali
        //pada Controller dan pada View diubah nama menjadi data
        return view('admin.product.index',['data'=>$queryBuilder]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //tampilkan data kategori sebagai foreign key
        $datakategori = Category::all();
        return view('admin.product.createform', compact('datakategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->file('image')->store('imageProducts');

        //untuk validasi saat user mengisi data, apabila ada yang masih kosong dimunculkan pesan error
        $request->validate([
            'nmProduk' => 'required',
            'hrgProduk' => 'required',
            'image' => 'required|image|file|max:10240',
            'deskProduk' => 'required',
            'ktgrProduk'  => 'required',
        ], [
            'nmProduk.required' => 'Nama produk tidak boleh kosong',
            'hrgProduk.required' => 'Harga produk tidak boleh kosong',
            'image.required' => 'Gambar produk tidak boleh kosong',
            'deskProduk.required' => 'Deskripsi produk tidak boleh kosong',
            'ktgrProduk.required'  => 'Kategori produk tidak boleh kosong',
        ]);             
        
        // New Produk berarti artinya new Data Kategori (insert)
        $data = new Product();

        // $data->[nama_kolom_pada_db] = $request->get('[name dari input text]')
        $data->nama_produk = $request->get('nmProduk');
        $data->harga_produk = $request->get('hrgProduk');
        if($request->file('image')) {
            $data['gambar_produk'] = $request->file('image')->store('imageProducts');
        }
        $data->deskripsi_produk = $request->get('deskProduk');
        $data->ketersediaan = $request->get('ktrsProduk');
        $data->tampil = $request->get('tplProduk');
        $data->categories_id = $request->get('ktgrProduk');
        $data->save();
        
        //akan memanggil route produk index yang akan kembali ke halaman utama Produk 
        //dengan menambahkan session status yang isinya berhasil ditambahkan Produk baru
        //with = membuat session sementara bernama status yg menampilkan berhasilnya ditambahkan data Produk 
        return redirect()->route('products.index')->with('status','Data Produk berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $data = $product;
        $datakategori = Category::all();
        return view('admin/product/editform',compact('data', 'datakategori'));
    }

    public function update(Request $request, Product $product)
    {
        $request->validate([
            'nmProduk' => 'required',
            'hrgProduk' => 'required',
            'image' => 'image|file|max:10240',
            'deskProduk' => 'required',
            'ktgrProduk'  => 'required',
        ], [
            'nmProduk.required' => 'Nama produk tidak boleh kosong',
            'hrgProduk.required' => 'Harga produk tidak boleh kosong',
            'deskProduk.required' => 'Deskripsi produk tidak boleh kosong',
            'ktgrProduk.required'  => 'Kategori produk tidak boleh kosong',
        ]);

        $product->nama_produk = $request->get('nmProduk');
        $product->harga_produk = $request->get('hrgProduk');
        if($request->file('image')) {
            if($request->oldImage) {
                Storage::delete($request->oldImage);
            }
            $product['gambar_produk'] = $request->file('image')->store('imageProducts'); 
        }
        $product->deskripsi_produk = $request->get('deskProduk');
        $product->ketersediaan = $request->get('ktrsProduk');
        $product->tampil = $request->get('tplProduk');
        $product->categories_id = $request->get('ktgrProduk');
        $product->save();
        
        return redirect()->route('products.index')->with('status', 'Data produk berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if($product->gambar_produk) { //delete file gambar
            Storage::delete($product->gambar_produk);
        }
        $product->delete();
        return redirect()->route('products.index')->with('statushapus', 'Data produk berhasil dihapus');
    }
}
