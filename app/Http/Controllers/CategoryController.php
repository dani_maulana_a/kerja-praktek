<?php

namespace App\Http\Controllers;
use App\Category;
use App\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Query dengan model
        // tampil pada halaman kategori admin
        return view('admin.category.index',['data'=>Category::all()]); //yg dikirim model nya. all() artinya array
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.createform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->file('image')->store('imageCategories');

        //untuk validasi saat user mengisi data, apabila ada yang masih kosong dimunculkan pesan error
        $request->validate([
            'nmKategori' => 'required',
            'image' => 'required|image|file|max:10240', //max 10mb (10240kb)
            'deskKategori' => 'required',
        ], [
            'nmKategori.required' => 'Nama kategori tidak boleh kosong',
            'image.required' => 'Gambar kategori tidak boleh kosong',
            'deskKategori.required' => 'Deskripsi kategori tidak boleh kosong',
        ]);     
        
        //New Kategori berarti artinya new Data Kategori (insert)
        $data = new Category();

        //$data->[nama_kolom_pada_db] = $request->get('[name dari input text]')
        $data->nama_kategori = $request->get('nmKategori');
        if($request->file('image')) {
            $data['gambar_kategori'] = $request->file('image')->store('imageCategories');
        }
        $data->deskripsi_kategori = $request->get('deskKategori');
        $data->save();
        
        //akan memanggil route kategori index yang akan kembali ke halaman utama Kategori 
        //dengan menambahkan session status yang isinya berhasil ditambahkan Kategori baru
        //with = membuat session sementara bernama status yg menampilkan berhasilnya ditambahkan data Kategori 
        return redirect()->route('categories.index')->with('status','Data Kategori berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $data = $category;
        return view('admin/category/editform',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //untuk validasi saat user mengisi data, apabila ada yang masih kosong dimunculkan pesan error
        $request->validate([
            'nmKategori' => 'required',
            'image' => 'image|file|max:10240', //max 10mb (10240kb)
            'deskKategori' => 'required',
        ], [
            'nmKategori.required' => 'Nama kategori tidak boleh kosong',
            'deskKategori.required' => 'Deskripsi kategori tidak boleh kosong',
        ]);     

        $category->nama_kategori = $request->get('nmKategori');
        if($request->file('image')) {
            if($request->oldImage) { //jika ada gambar lama di delete dulu
                Storage::delete($request->oldImage);
            }
            $category['gambar_kategori'] = $request->file('image')->store('imageCategories'); //upload gambar baru
        }
        $category->deskripsi_kategori = $request->get('deskKategori');
        $category->save();
        
        return redirect()->route('categories.index')->with('status', 'Data kategori berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        try {
            if($category->gambar_kategori) { //delete file gambar
                Storage::delete($category->gambar_kategori);
            }
            $category->delete();
            return redirect()->route('categories.index')->with('statushapus', 'Data kategori berhasil dihapus');
        } catch (\PDOException $e) {
            $msg = $this->handleAllRemoveChild($category);
            return redirect()->route('categories.index')->with('statushapus',
                $msg);
        }
    }
    //ini untuk menghapus data child (product)
    private function handleAllRemoveChild($s) {
        $s->products()->delete();
        $s->delete();
        return "Data kategori berhasil dihapus beserta data produk yang berkaitan";
    }
}
