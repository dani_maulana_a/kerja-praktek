<?php

namespace App\Http\Controllers;

use App\Visitor;
// use App\Helpers\UserSystemInfoHelper;
use Routes\web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use Illuminate\Routing\Route;
use DB;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // menyiapkan data untuk chart
        $periode = array();
        $data_seluruh = array();
        $data_seluruh_m = array();
        $data_seluruh_ip = array();
        $data_seluruh_session = array();
        $data_home = array();
        $data_home_ip = array();
        $data_home_session = array();
        $data_kategori = array();
        $data_kategori_ip = array();
        $data_kategori_session = array();
        $data_produk = array();
        $data_produk_ip = array();
        $data_produk_session = array();
        $data_kontak = array();
        $data_kontak_ip = array();
        $data_kontak_session = array();
        $categories = array();

        for ($i=1; $i <=5 ; $i++) {
            $periode = date("M Y", strtotime("-$i month", strtotime("+1 month", strtotime('now'))));
            array_push($categories, $periode);
            
            $seluruh_m = Visitor::where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get();
            array_push($data_seluruh_m, $seluruh_m->count());

            $seluruh_ip = Visitor::where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get()->groupBy('ip');
            array_push($data_seluruh_ip, $seluruh_ip->count());

            $seluruh_session = Visitor::where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get()->groupBy('session');
            array_push($data_seluruh_session, $seluruh_session->count());

            $home = Visitor::where('halaman', 'home customer')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get();
            array_push($data_home, $home->count());

            $homeip = Visitor::where('halaman', 'home customer')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get()->groupBy('ip');
            array_push($data_home_ip, $homeip->count());

            $homesession = Visitor::where('halaman', 'home customer')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get()->groupBy('session');
            array_push($data_home_session, $homesession->count());

            $kategori = Visitor::where('halaman', 'kategoricust')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get();
            array_push($data_kategori, $kategori->count());

            $kategoriip = Visitor::where('halaman', 'kategoricust')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get()->groupBy('ip');
            array_push($data_kategori_ip, $kategoriip->count());

            $kategorisession = Visitor::where('halaman', 'kategoricust')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get()->groupBy('session');
            array_push($data_kategori_session, $kategorisession->count());

            $produk = Visitor::where('halaman', 'produkcust')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get();
            array_push($data_produk, $produk->count());

            $produkip = Visitor::where('halaman', 'produkcust')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get()->groupBy('ip');
            array_push($data_produk_ip, $produkip->count());

            $produksession = Visitor::where('halaman', 'produkcust')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get()->groupBy('session');
            array_push($data_produk_session, $produksession->count());

            $kontak = Visitor::where('halaman', 'kontakcust')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get();
            array_push($data_kontak, $kontak->count());

            $kontakip = Visitor::where('halaman', 'kontakcust')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get()->groupBy('ip');
            array_push($data_kontak_ip, $kontakip->count());

            $kontaksession = Visitor::where('halaman', 'kontakcust')->where('time', '>=', date("Y-m-01", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->where('time', '<=',date("Y-m-t", strtotime("-$i month", strtotime("+1 month", strtotime('now')))))->get()->groupBy('session');
            array_push($data_kontak_session, $kontaksession->count());
        }
        // dd(json_encode($data_home_session));
        array_push($data_seluruh, ['name'=>'Murni', 'data'=>array_reverse($data_seluruh_m)],['name'=>'Berdasarkan IP', 'data'=>array_reverse($data_seluruh_ip)],['name'=>'Berdasarkan Session', 'data'=>array_reverse($data_seluruh_session)], ['name'=>'Home', 'data'=>array_reverse($data_home)], ['name'=>'Home IP', 'data'=>array_reverse($data_home_ip)], ['name'=>'Home Session', 'data'=>array_reverse($data_home_session)], ['name'=>'Kategori', 'data'=>array_reverse($data_kategori)], ['name'=>'Kategori IP', 'data'=>array_reverse($data_kategori_ip)], ['name'=>'Kategori Session', 'data'=>array_reverse($data_kategori_session)],  ['name'=>'Produk', 'data'=>array_reverse($data_produk)], ['name'=>'Produk IP', 'data'=>array_reverse($data_produk_ip)], ['name'=>'Produk Session', 'data'=>array_reverse($data_produk_session)], ['name'=>'Kontak', 'data'=>array_reverse($data_kontak)], ['name'=>'Kontak IP', 'data'=>array_reverse($data_kontak_ip)], ['name'=>'Kontak Session', 'data'=>array_reverse($data_kontak_session)]);

        return view('admin.welcome', compact('data_seluruh_m', 'data_seluruh_ip', 'data_seluruh_session', 'data_home', 'data_home_ip', 'data_home_session', 'data_kategori', 'data_kategori_ip', 'data_kategori_session', 'data_produk', 'data_produk_ip', 'data_produk_session', 'data_kontak', 'data_kontak_ip', 'data_kontak_session', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function show(Visitor $visitor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function edit(Visitor $visitor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visitor $visitor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visitor $visitor)
    {
        //
    }
}
