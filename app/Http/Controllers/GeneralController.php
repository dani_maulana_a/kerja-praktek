<?php

namespace App\Http\Controllers;

use App\General;
use Illuminate\Http\Request;
use DB;

class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.general.index',['data'=>General::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function show(General $general)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function edit(General $general)
    {
        $data = $general;
        return view('admin/general/editform',compact('data'));
    }

    public function update(Request $request, General $general)
    {
        $request->validate([
            'almtGeneral' => 'required',
            'almturlGeneral' => 'required',
            'notelpGeneral' => 'required',
            'emailGeneral' => 'required',
        ], [
            'almtGeneral.required' => 'Alamat tidak boleh kosong',
            'almturlGeneral.required' => 'URL alamat tidak boleh kosong',
            'notelpGeneral.required' => 'Nomor telepon tidak boleh kosong',
            'emailGeneral.required' => 'E-Mail tidak boleh kosong',
        ]);     
        
        $general->alamat = $request->get('almtGeneral');
        $general->alamat_url = $request->get('almturlGeneral');
        $general->no_telp = $request->get('notelpGeneral');
        $general->email = $request->get('emailGeneral');
        $general->save();
        
        return redirect()->route('generals.index')->with('status', 'Data kontak berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function destroy(General $general)
    {
        //
    }
}
