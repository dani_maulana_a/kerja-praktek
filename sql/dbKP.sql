-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2022 at 09:54 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbkp`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `idcategories` int(11) NOT NULL,
  `nama_kategori` varchar(45) NOT NULL,
  `gambar_kategori` varchar(100) DEFAULT NULL,
  `deskripsi_kategori` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`idcategories`, `nama_kategori`, `gambar_kategori`, `deskripsi_kategori`) VALUES
(1, 'T-Shirt Design', 'imageCategories/St1YI38PjtwzjHvSwWCKD7t8CkFaETuAwYnsSuDY.png', 'Desain t-shirt terbaik'),
(2, 'Poster', 'imageCategories/5DG1gLOwjNydedAf7zeDS4xBJ14JG17ENbhpC3xB.png', 'Media promosi ampuh'),
(3, 'Banner', 'imageCategories/KOb1foOTvjgNNXCs5SmHQFfIQcYUdPR35KJvMCaG.png', 'Meriahkan kegiatan Anda'),
(16, 'Sticker', 'imageCategories/yXa9luxo4pINZo5M0EnO6YomTAV8mtdyTFa7YJDj.png', 'Hias barang kesayangan Anda');

-- --------------------------------------------------------

--
-- Table structure for table `generals`
--

CREATE TABLE `generals` (
  `idgenerals` int(11) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `alamat_url` varchar(255) NOT NULL,
  `no_telp` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `generals`
--

INSERT INTO `generals` (`idgenerals`, `alamat`, `alamat_url`, `no_telp`, `email`) VALUES
(1, 'Jl. Raya Menganti Karangan No.28, Babatan, Kec. Wiyung, Kota SBY, Jawa Timur 60227, Indonesia', 'https://maps.google.com/maps?q=CREATECH%20SURABAYA&t=&z=13&ie=UTF8&iwloc=&output=embed', '089678833232', 'admin@createch.id');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `idproducts` int(11) NOT NULL,
  `nama_produk` varchar(45) NOT NULL,
  `harga_produk` double NOT NULL,
  `gambar_produk` varchar(100) DEFAULT NULL,
  `deskripsi_produk` longtext NOT NULL,
  `ketersediaan` enum('tersedia','tidak tersedia') NOT NULL,
  `tampil` enum('ya','tidak') NOT NULL,
  `categories_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`idproducts`, `nama_produk`, `harga_produk`, `gambar_produk`, `deskripsi_produk`, `ketersediaan`, `tampil`, `categories_id`) VALUES
(1, 'Kaos Keren', 100000, 'imageProducts/VcibNacoM3eFMdj8mblbslZgyiqCNbfLR91EpF9n.png', 'Kaos dengan bahan terbaik', 'tersedia', 'ya', 1),
(2, 'Quotes', 120000, 'imageProducts/K9madQZRlPoYt8q4z2PpC8IeE1naWGDQrCmn0RDV.png', 'Kalimat untuk mengembangkan diri', 'tersedia', 'ya', 2),
(6, 'Stiker Warna Warni', 100000, 'imageProducts/Znb2SfC82LBsuMkV8fjUqQWHjZLedcwyCHZaULHI.png', 'Mewarnai Hari-harimu', 'tidak tersedia', 'tidak', 16),
(15, 'Banner Acara', 100000, 'imageProducts/rehmOf3d5LpGi9ChksagNVV710d50imLA7SNAyyP.png', 'Menjadikan acara lebih berwarna', 'tersedia', 'ya', 3),
(24, 'Monokrom', 1000, 'imageProducts/h23XGZpy07n8JbFKpEmMDiRkTjRaOI4N74u5u8LN.png', 'Khusus warna dasar', 'tidak tersedia', 'ya', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'rafly', 'rafly@gmail.com', NULL, '$2y$10$2RjclP/XXdre5BqGoRbo.emLg97IOH5e3kmO7jh78DUefxoEi4Ida', NULL, '2022-02-04 08:58:07', '2022-02-04 08:58:07');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `idvisitors` int(11) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `session` varchar(100) DEFAULT NULL,
  `halaman` varchar(45) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`idvisitors`, `ip`, `session`, `halaman`, `time`) VALUES
(1, '127.0.0.1', NULL, NULL, '2022-02-26 08:46:45'),
(2, '127.0.0.1', NULL, NULL, '2022-03-02 09:22:43'),
(3, '127.0.0.1', NULL, NULL, '2022-03-02 09:57:31'),
(4, '127.0.0.1', '57cdkvhuk5n2rfs5nlflritc9a', NULL, '2022-03-02 14:22:32'),
(5, '127.0.0.1', '57cdkvhuk5n2rfs5nlflritc9a', NULL, '2022-03-02 14:52:26'),
(6, '127.0.0.1', '57cdkvhuk5n2rfs5nlflritc9a', NULL, '2022-03-02 15:49:08'),
(7, '127.0.0.1', '57cdkvhuk5n2rfs5nlflritc9a', NULL, '2022-03-02 15:49:09'),
(8, '127.0.0.1', '57cdkvhuk5n2rfs5nlflritc9a', 'home customer', '2022-03-26 13:35:09'),
(9, '127.0.0.1', 'iln3klj08bcgmjgh0qc50i58vf', 'home customer', '2022-03-26 13:34:54'),
(10, '127.0.0.1', 'iln3klj08bcgmjgh0qc50i58vf', 'home customer', '2022-03-26 13:34:35'),
(11, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'home customer', '2022-03-26 13:34:28'),
(12, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'home customer', '2022-03-26 13:34:10'),
(13, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'home customer', '2022-03-26 13:34:00'),
(14, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'home customer', '2022-03-11 09:59:53'),
(15, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'home customer', '2022-03-11 10:15:05'),
(16, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'home customer', '2022-03-11 10:15:44'),
(17, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'home customer', '2022-03-11 10:16:05'),
(18, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'kategoricust', '2022-03-11 10:27:24'),
(19, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'kategoricust', '2022-03-11 10:27:36'),
(20, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'produkcust', '2022-03-11 10:29:06'),
(21, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'kontakcust', '2022-03-11 10:29:14'),
(22, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'kategoricust', '2022-03-11 10:38:13'),
(23, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'produkcust', '2022-03-11 10:45:40'),
(24, '127.0.0.1', 'h7863jpca1rg0upt095mtmnbmq', 'kontakcust', '2022-03-11 10:57:41'),
(25, '127.0.0.1', 's2tbqjduv209it0osif1or8rq1', 'home customer', '2022-03-22 07:26:42'),
(26, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 07:26:44'),
(27, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 08:23:41'),
(28, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 08:25:38'),
(29, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 08:26:06'),
(30, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 08:27:01'),
(31, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 16:59:56'),
(32, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:00:50'),
(33, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:01:33'),
(34, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:02:15'),
(35, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:02:51'),
(36, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:03:52'),
(37, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:04:07'),
(38, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:04:39'),
(39, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:04:56'),
(40, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:05:40'),
(41, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:06:27'),
(42, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:15:21'),
(43, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:15:52'),
(44, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:26:38'),
(45, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:30:27'),
(46, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:31:27'),
(47, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:31:40'),
(48, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:32:02'),
(49, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:32:46'),
(50, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:33:03'),
(51, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:33:18'),
(52, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:33:27'),
(53, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:33:34'),
(54, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:36:25'),
(55, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:37:48'),
(56, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:38:28'),
(57, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:39:06'),
(58, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:39:15'),
(59, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:47:00'),
(60, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:47:32'),
(61, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:47:37'),
(62, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:48:20'),
(63, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:48:50'),
(64, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 17:49:08'),
(65, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:57:14'),
(66, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:57:25'),
(67, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'produkcust', '2022-03-22 17:57:39'),
(68, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 17:57:53'),
(69, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'kategoricust', '2022-03-22 18:00:41'),
(70, '127.0.0.1', 'blbe9ao2nsrnpgnl1401nlol76', 'home customer', '2022-03-22 18:07:03'),
(71, '127.0.0.1', 'earounbc3u10allf6mkrc407rl', 'home customer', '2022-03-23 16:46:32'),
(72, '127.0.0.1', 'i3bqpims15c2g4420mrmse19v1', 'home customer', '2022-03-24 06:47:02'),
(73, '127.0.0.1', 'i3bqpims15c2g4420mrmse19v1', 'kategoricust', '2022-03-24 06:47:19'),
(74, '127.0.0.1', 'i3bqpims15c2g4420mrmse19v1', 'kategoricust', '2022-03-24 06:48:46'),
(75, '127.0.0.1', 'i3bqpims15c2g4420mrmse19v1', 'kategoricust', '2022-03-24 06:53:13'),
(76, '127.0.0.1', 'ilh1js3orlseh74lnkqi7j3cmb', 'home customer', '2022-03-26 10:39:10'),
(77, '127.0.0.1', '5j81djmsi3rrksm1scga36mnep', 'home customer', '2022-03-28 08:24:54'),
(78, '127.0.0.1', '5j81djmsi3rrksm1scga36mnep', 'home customer', '2022-03-28 08:25:17'),
(79, '127.0.0.1', '7j019ppr614pt493fbimui958f', 'home customer', '2022-04-03 08:03:46'),
(80, '127.0.0.1', 'lv337h35j3ig4s11qit9i07qka', 'home customer', '2022-04-07 09:17:11'),
(81, '127.0.0.1', 'gohsuofjtvllobgad5a200o7o8', 'home customer', '2022-04-19 09:57:07'),
(82, '127.0.0.1', 'gohsuofjtvllobgad5a200o7o8', 'home customer', '2022-04-19 10:01:28'),
(83, '127.0.0.1', 'gohsuofjtvllobgad5a200o7o8', 'home customer', '2022-04-19 10:10:23'),
(84, '127.0.0.1', 'gohsuofjtvllobgad5a200o7o8', 'home customer', '2022-04-19 10:14:48'),
(85, '127.0.0.1', 'gohsuofjtvllobgad5a200o7o8', 'home customer', '2022-04-19 10:15:26'),
(86, '127.0.0.1', 'gohsuofjtvllobgad5a200o7o8', 'produkcust', '2022-04-19 10:15:42'),
(87, '127.0.0.1', 'gohsuofjtvllobgad5a200o7o8', 'home customer', '2022-04-19 10:15:56'),
(88, '127.0.0.1', 'gohsuofjtvllobgad5a200o7o8', 'home customer', '2022-04-19 10:18:03'),
(89, '127.0.0.1', 'gohsuofjtvllobgad5a200o7o8', 'home customer', '2022-04-19 10:21:25'),
(90, '127.0.0.1', 'gohsuofjtvllobgad5a200o7o8', 'home customer', '2022-04-19 14:50:19'),
(91, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 08:46:48'),
(92, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 08:51:11'),
(93, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 08:55:28'),
(94, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:03:27'),
(95, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:06:11'),
(96, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:08:13'),
(97, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:10:11'),
(98, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:10:45'),
(99, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:10:52'),
(100, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:11:00'),
(101, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:11:08'),
(102, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:17:35'),
(103, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:17:48'),
(104, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:18:38'),
(105, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:18:43'),
(106, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:18:50'),
(107, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:18:55'),
(108, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'kategoricust', '2022-04-25 09:19:07'),
(109, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:19:23'),
(110, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:19:43'),
(111, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:19:50'),
(112, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:20:09'),
(113, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:20:29'),
(114, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'kategoricust', '2022-04-25 09:20:40'),
(115, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'kategoricust', '2022-04-25 09:20:47'),
(116, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'kategoricust', '2022-04-25 09:20:51'),
(117, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'kategoricust', '2022-04-25 09:21:01'),
(118, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'produkcust', '2022-04-25 09:24:04'),
(119, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'kategoricust', '2022-04-25 09:24:51'),
(120, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:24:54'),
(121, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:25:02'),
(122, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'kontakcust', '2022-04-25 09:25:17'),
(123, '127.0.0.1', 'kjns1af16k20n0f8aukn0q1mlv', 'home customer', '2022-04-25 09:25:29'),
(124, '127.0.0.1', '5dc6u0qjrbo3gni1fiihvca28r', 'home customer', '2022-04-27 07:37:13'),
(125, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'home customer', '2022-05-10 05:34:41'),
(126, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'home customer', '2022-05-10 05:35:03'),
(127, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'home customer', '2022-05-10 05:35:07'),
(128, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'home customer', '2022-05-10 05:35:44'),
(129, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'home customer', '2022-05-10 05:36:47'),
(130, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'home customer', '2022-05-10 05:38:46'),
(131, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'home customer', '2022-05-10 09:08:31'),
(132, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'home customer', '2022-05-10 10:33:52'),
(133, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'home customer', '2022-05-10 10:35:42'),
(134, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'home customer', '2022-05-10 10:53:44'),
(135, '127.0.0.1', '492rg751llfea06kt9i6jokd17', 'produkcust', '2022-05-10 10:53:52'),
(136, '127.0.0.1', 'cirhcv3rqu3h5833fir9niqjc3', 'home customer', '2022-05-12 16:03:48'),
(137, '127.0.0.1', 'cirhcv3rqu3h5833fir9niqjc3', 'home customer', '2022-05-12 16:04:51'),
(138, '127.0.0.1', 'cirhcv3rqu3h5833fir9niqjc3', 'home customer', '2022-05-12 16:13:20'),
(139, '127.0.0.1', 'iu7bpru8ke5n1mfg7eohqe1i5k', 'home customer', '2022-05-26 10:17:45'),
(140, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 15:34:50'),
(141, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 15:35:55'),
(142, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 15:36:07'),
(143, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 15:56:31'),
(144, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 16:03:41'),
(145, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 17:51:06'),
(146, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 17:56:28'),
(147, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 17:57:33'),
(148, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 17:58:35'),
(149, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 17:59:27'),
(150, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'kategoricust', '2022-06-02 17:59:30'),
(151, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'produkcust', '2022-06-02 17:59:52'),
(152, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'produkcust', '2022-06-02 18:00:06'),
(153, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'kategoricust', '2022-06-02 18:00:11'),
(154, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'kontakcust', '2022-06-02 18:00:15'),
(155, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 18:00:22'),
(156, '127.0.0.1', 's8o7dhf0asagstjs23kl42fda8', 'home customer', '2022-06-02 18:00:45'),
(157, '127.0.0.1', 'uc3u1dfs1l5ahvde96oko3suk5', 'home customer', '2022-06-06 07:08:54'),
(158, '127.0.0.1', 'uc3u1dfs1l5ahvde96oko3suk5', 'home customer', '2022-06-06 07:15:08'),
(159, '127.0.0.1', 'uc3u1dfs1l5ahvde96oko3suk5', 'produkcust', '2022-06-06 07:16:07'),
(160, '127.0.0.1', 'uc3u1dfs1l5ahvde96oko3suk5', 'home customer', '2022-06-06 07:17:39'),
(161, '127.0.0.1', 'uc3u1dfs1l5ahvde96oko3suk5', 'home customer', '2022-06-06 07:18:20'),
(162, '127.0.0.1', 'uc3u1dfs1l5ahvde96oko3suk5', 'home customer', '2022-06-06 07:18:33'),
(163, '127.0.0.1', 'uc3u1dfs1l5ahvde96oko3suk5', 'produkcust', '2022-06-06 07:18:40'),
(164, '127.0.0.1', 'uc3u1dfs1l5ahvde96oko3suk5', 'home customer', '2022-06-06 07:19:11'),
(165, '127.0.0.1', 'uc3u1dfs1l5ahvde96oko3suk5', 'home customer', '2022-06-06 07:19:16'),
(166, '127.0.0.1', 'uc3u1dfs1l5ahvde96oko3suk5', 'produkcust', '2022-06-06 07:19:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`idcategories`);

--
-- Indexes for table `generals`
--
ALTER TABLE `generals`
  ADD PRIMARY KEY (`idgenerals`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`idproducts`),
  ADD KEY `fk_products_categories_idx` (`categories_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`idvisitors`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `idcategories` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `generals`
--
ALTER TABLE `generals`
  MODIFY `idgenerals` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `idproducts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `idvisitors` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_categories` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`idcategories`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
